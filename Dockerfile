FROM nixos/nix

RUN nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
RUN nix-channel --update

COPY . /repro

RUN nix-shell -E "with import <nixpkgs> {}; callPackage /repro/default.nix {}" --show-trace
